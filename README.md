## OpenCL GPU Test for Iterative Solvers
# Requirements:
+ OpenCL >= 1.2
+ OpenMP (optional)
+ Boost >= 1.65
+ CMake >= 3.10

Windows Only:
+ MS Visual Studio. You can download MSVS here: [MSVS](https://visualstudio.microsoft.com/de/vs/)
+ Therefore: choose Download + `Community Edition`

# Install: 

+ Make sure to install OpenCL Developer Version for your operating system.
For NVIDIA GPUs use [CUDA](https://developer.nvidia.com/cuda-downloads) and 
download CUDA v.10 for your Operating System and CPU. 
This should also install OpenCL, *make sure to check the according CheckBox if you're using the installer*. 
        

+ For Windows: Download Boost Library Installer [Boost](https://sourceforge.net/projects/boost/files/boost-binaries/1.71.0/)
    for your Visual Studio Version. *Make sure to use the 64Bit Version*.
+ Download CMAKE for Windows if not already installed: [CMAKE](https://cmake.org/download/)

# CMake Process GUI

+ Navigate to the *tinypack* folder, which you just downloaded
+ create a folder called 'build' inside of `tinypack/Analysis/factor_matrix/`
+ Use CMake by following the instructions here [CMAKE Instructions](https://cmake.org/runningcmake/).
    - In the field: "Where is the source" choose the folder `tinypack/Analysis/factor_matrix` which contains 'CMakeLists.txt'
    - For the build folder choose the `tinypack/Analysis/factor_matrix/build` folder you just created
    - Press 'Configure': you should now see CMake detecting your compilers, libs and stuff
    - Add the following entries: 
        + `CMAKE_BUILD_TYPE=Release `
        + `USE_VCL=ON `
        + `HAVE_OPENMP=ON` (optional)
    - Press 'Generate' to generate the Visual Studio Project file.
    - There should now be a Visual Studio Project file in the 'build' folder which you can open.
    - Now Compile in VS GUi. 

If CMake cannot find Boost, try setting an Environment variable called 'BOOSTROOT'
that points to the Boost install folder of yours, where the folders lib and include are located.

# CMake Process NO GUI
Alternatively without using the GUI: 
+ Open windows Explorer and go to the folder 
+ type: 

```
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DUSE_VCL=ON -DHAVE_OPENCL=ON -DHAVE_OPENMP=ON
```

This should also produce a VS Project that you can open and compile the code with. 
On Unix hit : 
```
make -j8 factor_cl
```


# Test Prepare

+ Make sure your PC does not switch off due to inactivity. If *Energy Saving* is activated, please deactivate it
  for the following test. Otherwise, the measurements are corrupted.
+ During the Test you should not use you PC with heavy weight programs. Ideally, `stop all heavy programs` 
  such as office applications or your browser.

# Running the test:

+ Open windows Explorer and go to the folder that contains the file "factor_cl", which is the 
  program you built.
+ Then press Shift + right click mouse button any where on the folder window and hit 'Open Windows Command window here'
+ Copy the following line:
  `./factor_cl.exe -rs 100 -re 7100 -step 100 -n 3 -name GPUTest_NOSPAI -H Eigen -I SPAI -I FSPAI `
  *after the name flag, please enter some identifier for your GPU card, without using spaces*.
+ right click in the windows command window and press past (*CTRL + V does* **not** *Work in Windows Shell*)
+ **Go for a Coffee, as the Test will run some time** :)


