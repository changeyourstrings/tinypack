#pragma once
#include "boost/numeric/ublas/matrix_sparse.hpp"
#include "eigen_types.hpp"
#include "vcl_types.hpp"

namespace vcl {
eigen::SM conv(const M_t &A, const V_t &V);
eigen::SM conv(const M_t &A, bool add_ones = false);
M_t conv(const eigen::SM &A);

using BoostCM = boost::numeric::ublas::compressed_matrix<double>;
BoostCM conv2BoostCM(const M_t &A);
eigen::SM conv(const BoostCM &A);

} // namespace vcl