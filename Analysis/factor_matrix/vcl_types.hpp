#pragma once

#include "eigen_types.hpp"
#include "fm/dprint.hpp"

#define VIENNACL_WITH_EIGEN 1
#ifdef VIENNACL_WITH_OPENCL
#define CL_SILENCE_DEPRECATION
#include "viennacl/device_specific/builtin_database/common.hpp"
#include "viennacl/ocl/device.hpp"
#include "viennacl/ocl/platform.hpp"
#endif

#include "viennacl/compressed_matrix.hpp"
#include "viennacl/vector.hpp"

namespace vcl {
using M_t = viennacl::compressed_matrix<double>;
using V_t = viennacl::vector<double>;
// From official viennaCL doc
static void print_devices() {
#if USE_VCL && defined(VIENNACL_WITH_OPENCL)

  auto platforms = viennacl::ocl::get_platforms();
  bool is_first_element = true;

  for (auto platform_iter = platforms.begin(); platform_iter != platforms.end();
       ++platform_iter) {

    auto devices = platform_iter->devices(CL_DEVICE_TYPE_ALL);

    dprint_x("\n\n# =========================================");
    dprint_x("#         GPU Devices Information             ");
    dprint_x("# =========================================\n\n");
    dprint("# Vendor and version: " << platform_iter->info());
    dprint("# Available Devices: \n");
    try {
      for (auto it = devices.begin(); it != devices.end(); it++) {
        dprint("#  -----------------------------------------");
        dprint(it->full_info());
        dprint(
            "# ViennaCL Device Architecture:   " << it->architecture_family());
        dprint("# ViennaCL Database Mapped Name: "
               << viennacl::device_specific::builtin_database::
                      get_mapped_device_name(it->name(), it->vendor_id()));
        dprint("#  -----------------------------------------");
      }
    } catch (viennacl::ocl::invalid_value &ex) {
      dprint("Error: Cannot print devices, viennaCL reported:\n "
             << "ex.what()" << "\n Skipping print.");
    }
    dprint("\n###########################################\n");
  }
#else
  dprint_x("Using CPU based computations.");
#endif
}
} // namespace vcl
