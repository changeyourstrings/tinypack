#pragma once

#define VIENNACL_WITH_EIGEN 1

#ifdef VIENNACL_WITH_OPENCL
#define CL_SILENCE_DEPRECATION
#endif

#include "fm.hpp"
#include "vcl_types.hpp"
#include "vienna_cl_conv.hpp"
#include "viennacl/linalg/ilu.hpp"
#include "viennacl/linalg/norm_2.hpp"

template <> inline void to_vector(const stl_vec &v, vcl::V_t &r) {
  r = vcl::V_t(v.size());
  viennacl::copy(v, r);
}

template <> inline void to_matrix(const recordings &R, vcl::M_t &A) {
  auto Q = R.to_eigen_SM();
  // causes an nullptr exception in viennacl;
  //  A = vcl::M_t(R.system_size, R.system_size);
  viennacl::copy(Q, A);
}

namespace vcl {
class vcl_test : public Test<M_t, V_t> {

  int sweeps = 3;
  int jacobi_iters = 3;

public:
  vcl_test(int size);

  const char *name() const { return "VCL"; }

  void run(recordings &, test_collection_t &coll, test_methods &ms);
  int get_row_size() const;
  comp ilut();
  comp ilu0();
  // comp ilublock();
  comp mul();
  comp chowilu();
  comp chowicc();
#if VIENNACL_WITH_OPENCL
  comp spai();
  comp fspai();
#endif

  template <class solver_tag, class solver_type, class... Args>
  comp test_pcIterative(Args &&... args) {
    using namespace viennacl;
    using namespace viennacl::linalg;

    V_t q;
    solver_type solver(
        solver_tag(TestRepeater::TOLERANCE, TestRepeater::MAX_ITERATIONS));

    ms t = time_it([this, &solver, &q]() {
      if (TestRepeater::PRECOND == AUT::ILUT) {
        ilut_precond<M_t> PC(_A, ilut_tag(get_row_size()));
        q = solver(_A, _b, PC);
      } else {
        chow_patel_ilu_precond<M_t> PC(_A, chow_patel_tag(sweeps));
        q = solver(_A, _b, PC);
      }
    });

    return comp{t, solver.tag().error(), solver.tag().iters(), norm_2(q - _x)};
  }

private:
  template <class Matrix>
  double error_SPAI(int _size, const M_t &_A, const Matrix &M) {
    return (eigen::Mat::Identity(_size, _size) - conv(_A) * conv(M)).norm();
  }

  double error_LU(const eigen::SM &L, const eigen::SM &U, const eigen::SM &A) {
    return (eigen::Mat(A) - L * U).norm();
  }
};
} // namespace vcl
