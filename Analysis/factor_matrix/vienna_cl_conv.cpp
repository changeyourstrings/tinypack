#include "vienna_cl_conv.hpp"
#include <map>
#include <vector>
namespace vcl {
using stl_mtx = std::vector<std::map<unsigned int, double>>;

stl_mtx to_map(const M_t &A) {
  stl_mtx Q(A.size1());
  viennacl::copy(A, Q);
  return Q;
}

eigen::SM conv(const M_t &A, bool add_ones) {
  auto Q = to_map(A);

  std::vector<eigen::Triplet> tripls;

  tripls.reserve(Q.size());
  //#pragma omp parallel for
  for (int i = 0; i < Q.size(); i++) {
    for (auto &[j, v] : Q[i]) {
      tripls.emplace_back(i, j, v);
    }
  }

  eigen::SM X(Q.size(), Q.size());
  X.reserve(tripls.size());
  X.setFromTriplets(tripls.begin(), tripls.end());

  if (add_ones) {
    for (int i = 0; i < Q.size(); i++) {
      X.insert(i, i) = 1;
    }
  }

  return X;
}

M_t conv(const eigen::SM &A) {
  std::vector<std::map<unsigned int, double>> Q(A.outerSize());

  //#pragma omp parallel for
  for (int i = 0; i < A.outerSize(); i++) {
    typename eigen::SM::InnerIterator it(A, i);
    do {
      Q[i].emplace(it.index(), it.value());
    } while (++it);
  }

  M_t X(A.outerSize(), A.outerSize());
  viennacl::copy(Q, X);
  return X;
}

eigen::SM conv(const M_t &A, const V_t &V) {
  std::vector<std::map<unsigned int, double>> Q(A.size1());
  viennacl::copy(A, Q);

  std::vector<eigen::Triplet> tripls;
  tripls.reserve(Q.size() + V.size());
  //#pragma omp parallel for
  for (int i = 0; i < Q.size(); i++) {
    for (auto &[j, v] : Q[i]) {
      tripls.emplace_back(i, j, v);
    }
  }

  for (int i = 0; i < V.size(); i++) {
    tripls.emplace_back(i, i, V[i]);
  }

  eigen::SM X(Q.size(), Q.size());
  X.reserve(tripls.size());
  X.setFromTriplets(tripls.begin(), tripls.end());
  return X;
}

BoostCM conv2BoostCM(const M_t &A) {
  stl_mtx Q(A.size1());
  viennacl::copy(A, Q);

  BoostCM X(A.size1(), A.size1());
  X.reserve(A.nnz());
  for (int i = 0; i < Q.size(); i++) {
    for (auto &[j, v] : Q[i]) {
      X.push_back(i, j, v);
    }
  }
  return X;
}

eigen::SM conv(const BoostCM &A) {
  std::vector<eigen::Triplet> tripls;
  tripls.reserve(A.nnz());

  for (auto i1 = A.begin1(); i1 != A.end1(); ++i1) {
    for (auto i2 = i1.begin(); i2 != i1.end(); ++i2) {
      tripls.emplace_back(i2.index1(), i2.index1(), *i2);
    }
  }

  eigen::SM X(A.size1(), A.size1());
  X.reserve(tripls.size());
  X.setFromTriplets(tripls.begin(), tripls.end());

  return X;
}
} // namespace vcl