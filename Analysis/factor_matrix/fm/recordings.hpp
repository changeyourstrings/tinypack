#pragma once

#include "common_types.hpp"
#include "eigen_types.hpp"
#include <string>
#include <vector>

using str = std::string;
struct recordings; // forward

//***********************************************************************************************
// PARSING STUFF
//***********************************************************************************************

int extract_size(const str &line, int &e);

std::pair<str, str> get_row_and_col(str &line, int &e);

stl_vec read_spooles_matlab_output_vector(const str &fp, char pre,
                                          int n_max = -1);
double read_value(str &line, int &e);
int read_spooles_matlab_output(const str &fp, std::vector<int> &cols,
                               std::vector<int> &rows, stl_vec &vals, int &size,
                               int n_max = -1);

struct recordings {
  int system_size;
  std::vector<int> cols, rows;
  stl_vec vals, b, x;
  bool loaded_from_file = false;

public:
  recordings() = default;
  explicit recordings(int s, int fill_level);

  void set(int i, int r, int c, double v);
  void push(int r, int c, double v);

  double density() const;
  size_t non_zeros() const;

  static bool load(const str &file, int fnum, recordings &R, int N_MAX = -1);
  static void random(int size, double fill, recordings &R);
  static void random2(int size, double fill, recordings &R);

  std::vector<eigen::Triplet> to_triplets() const;
  eigen::SMColMaj to_eigen_SM_ColMajor() const;
  eigen::SM to_eigen_SM() const;
  stl_matrix to_map(bool force_skip=false) const;

  inline const stl_vec &get_x() const { return x; }
  inline const stl_vec &get_b() const { return b; }
  inline eigen::CMapXd get_x_eigen() const {
    return eigen::CMapXd(x.data(), system_size);
  }
  inline eigen::CMapXd get_b_eigen() const {
    return eigen::CMapXd(b.data(), system_size);
  }
};
