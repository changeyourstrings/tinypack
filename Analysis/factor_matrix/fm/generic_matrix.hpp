#pragma once
#include "common_types.hpp"
#include "recordings.hpp"

static stl_vec operator-(const stl_vec &b, const stl_vec &v);
static stl_vec operator*(const recordings &R, const stl_vec &v);

template <class V> inline stl_vec to_stl_vec(const V &v) {
  stl_vec s(v.size());
  for (size_t i = 0; i < v.size(); i++) {
    s.push_back(v[i]);
  }
  return s;
}

template <class V> inline stl_vec move_to_stl_vec(V &&v);

static stl_vec to_stl_vec(const double *v, int size) {
  stl_vec s(size);
  std::copy(v, v + size, s.begin());
  return s;
}

template <> inline stl_vec to_stl_vec<stl_vec>(const stl_vec &v) {
  return stl_vec(v);
}

template <> inline stl_vec move_to_stl_vec<stl_vec>(stl_vec &&v) {
  return stl_vec(std::move(v));
}

template <class Vector> inline void to_vector(const stl_vec &v, Vector &);
template <class Vector> inline void move_to_vector(stl_vec &&v, Vector &);

template <> inline void to_vector<stl_vec>(const stl_vec &v, stl_vec &u) {
  u = v;
}

template <class Matrix> inline void to_matrix(const recordings &R, Matrix &);

template <class Vector> inline double my_norm(const Vector &v);

template <class M> inline M matrix_init(int size) { return M(size, size); }
