#pragma once
#include "common_types.hpp"
#include "dprint.hpp"
#include "eigen_types.hpp"
#include "timing.hpp"

#include <map>
#include <set>
#include <string>
#include <vector>

// ALGORITHM UNDER TEST
namespace AUT {
constexpr const char *SET = "SET";
constexpr const char *CG = "CG";
constexpr const char *BIC = "BICGStab";
constexpr const char *GMRES = "GMRES";
constexpr const char *MUL = "MUL";
constexpr const char *LU = "LU";
constexpr const char *PARDISOLU = "PARDISOLU";
constexpr const char *ILUT = "ILUT";
constexpr const char *ILU0 = "ILU0";
constexpr const char *ICC = "ICC";
constexpr const char *DIAG = "DIAG";
constexpr const char *PARILU = "PARILU";
constexpr const char *PARICC = "PARICC";
constexpr const char *SPAI = "SPAI";
constexpr const char *FSPAI = "FSPAI";
}; // namespace AUT

struct measurements;

using str = std::string;
using sol_t = std::pair<const double *, int>;
using test_collection_t = std::map<str, std::vector<measurements>>;
using test_methods = std::vector<str>;

bool is_zero(double v);

void assert_is_positive_def(const eigen::SM &A, int size);
bool check_is_positive_def(const eigen::SM &A, int size, std::vector<std::complex<double>> &std_evs);
void assert_is_symmetric(const eigen::SM &A, int size);

/***************************************************************************************************/

class comp {
  ms _runtime = ms(0.);
  double _b_error = 0.; // error after performing Ax=b
  double _x_error = 0.; // error in solution x
  size_t _iterations = 0;
  bool _valid = false;
  stl_vec _sol;

public:
  comp() = default;
  explicit comp(ms runtime_, double error_, size_t iterations_ = 0,
                double x_error_ = .0);

  comp operator+(const comp &other) const;
  comp &operator/(double v);
  comp &mean(size_t s);

  double b_error() const { return _b_error; }
  double x_error() const { return _x_error; }

  void set_sol(stl_vec &&v) { _sol = v; }
  const stl_vec &sol() const { return _sol; }

  str to_csv() const;
  auto runtime() const { return _runtime; }
  auto iters() const { return _iterations; }
  friend std::ostream &operator<<(std::ostream &o, const comp &c);
};

std::ostream &operator<<(std::ostream &o, const comp &c);
std::ostream &operator<<(std::ostream &o, const sol_t &p);

// ***********************************************************************************************

struct measurements {
  std::vector<comp> _ms;

public:
  inline measurements(size_t s) : _ms(s) {}
  comp &operator[](int i);
  const comp &operator[](int i) const;
  comp mean() const;
  ms stddev_time() const;
  void add(comp &&c);
  template <class... Args> void emplace_back(Args &&... args) {
    _ms.emplace_back(std::forward<Args>(args)...);
  }
  size_t size() const;
};

// ***********************************************************************************************

struct TestRepeater {
  static double TOLERANCE;
  static double ILUT_S;
  static size_t MAX_ITERATIONS;
  static size_t MAX_THREADS;
  static bool VERBOSE;
  static bool SAFE;
  static bool NO_SPD;
  static str PRECOND;
  static size_t RUN_NTIMES;
  static std::set<str> masked, only;
  static std::map<str, std::set<str>> specific_masked, specific_only;
  static str cur_test;
  static void set_current_test(const str &s) { cur_test = s; }
  static void clear_test() { cur_test = ""; }

  static bool excluded(const str &s) {
    if (only.size() > 0 or specific_only.size() > 0) {
      if (only.find(s) != only.end()) {
        return false;
      } else if (cur_test != "" and
                 specific_only.find(cur_test) != specific_only.end()) {
        return specific_only[cur_test].find(s) == specific_only[cur_test].end();
      }
      return true;
    } else {
      if (masked.find(s) != masked.end()) {
        return true;
      } else if (cur_test != "" and
                 specific_masked.find(cur_test) != specific_masked.end()) {
        return specific_masked[cur_test].find(s) !=
               specific_masked[cur_test].end();
      }
      return false;
    }
  }

  template <class F>
  static void N_TIMES(const str &s, test_collection_t &collection,
                      test_methods &methods, F &&f) {

    if (TestRepeater::excluded(s)) {
      if (TestRepeater::VERBOSE)
        dprint_red("Test: '" << s << "' is excluded. ");
      return;
    } else {
      if (TestRepeater::VERBOSE)
        dprint_red("Running test: '" << s << "' " << RUN_NTIMES << " times...");
    }

    measurements results(TestRepeater::RUN_NTIMES);
    for (size_t i = 0; i < TestRepeater::RUN_NTIMES; i++) {
      results[i] = std::invoke(f);
    }

    collection[s].push_back(std::move(results));
    methods.push_back(s);
  }

  template <class F, class... Args>
  static void N_TIMES2(const str &s, test_collection_t &collection,
                       test_methods &methods, F &&f, Args &&... args) {

    if (TestRepeater::excluded(s)) {
      if (TestRepeater::VERBOSE)
        dprint_red("Test: '" << s << "' is excluded. ");
      return;
    } else {
      if (TestRepeater::VERBOSE)
        dprint_red("Running test: '" << s << "' " << RUN_NTIMES << " times...");
    }

    measurements results(TestRepeater::RUN_NTIMES);
    for (size_t i = 0; i < TestRepeater::RUN_NTIMES; i++) {
      results[i] = std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
    }

    collection[s].push_back(std::move(results));
    methods.push_back(s);
  }
};

// ***********************************************************************************************
