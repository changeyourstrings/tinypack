#pragma once

enum LibTag { EIGEN = 0, VCL = 1 };
enum SolverTag { BIC = 0, CG = 1, GMRES };
enum PCTag { ILUT = 0, PARILU = 1 , PARICC = 2};
