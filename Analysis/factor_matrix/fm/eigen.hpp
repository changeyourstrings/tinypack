#pragma once

#include "fm.hpp"
#include <unsupported/Eigen/IterativeSolvers>

#ifdef EIGEN_USE_MKL
#include <Eigen/PardisoSupport>
#endif

template <>
inline void to_matrix<eigen::SM>(const recordings &R, eigen::SM &A) {
  A = R.to_eigen_SM();
}

template <> inline void to_vector<eigen::Vec>(const stl_vec &v, eigen::Vec &x) {
  x = eigen::Vec(v.size());
#pragma omp parallel for
  for (size_t i = 0; i < v.size(); i++) {
    x[i] = v[i];
  }
}

template <> inline double my_norm(const eigen::Vec &v) { return v.norm(); }

namespace eigen {

template <class Solver, class MatrixType>
comp run_solver(const MatrixType &A, const Vec &x, const Vec &b) {

  Solver solver;
  solver.setTolerance(TestRepeater::TOLERANCE);
  solver.setMaxIterations(TestRepeater::MAX_ITERATIONS);

  eigen::Vec q;
  ms t = time_it([&A, &solver, &b, &q]() {
    solver.compute(A);
    q = solver.solve(b);
  });

  eigen::Vec Aq = A * q;
  auto error = (Aq - b).norm();
  auto x_error = (q - x).norm();

  comp r(t, error, solver.iterations(), x_error);
  return r;
}

template <class Solver, class MatrixType>
comp run_solver_sol(const MatrixType &A, const Vec &x, const Vec &b) {

  Solver solver;
  solver.setTolerance(TestRepeater::TOLERANCE);
  solver.setMaxIterations(TestRepeater::MAX_ITERATIONS);
  eigen::Vec q;
  ms t = time_it([&A, &solver, &b, &q]() {
    solver.compute(A);
    q = solver.solve(b);
  });

  eigen::Vec Aq = A * q;
  auto error = (Aq - b).norm();
  auto x_error = (q - x).norm();

  comp c(t, error, solver.iterations(), x_error);
  c.set_sol(std::move(to_stl_vec(q)));
  return c;
}

template <class PC, class MatrixType>
void test_iterative(test_collection_t &C, test_methods &M, const MatrixType &A,
                    const Vec &x, const Vec &b) {

  using T = TestRepeater;
  using CG = Eigen::ConjugateGradient<SM, Eigen::Lower | Eigen::Upper, PC>;
  using BIC = Eigen::BiCGSTAB<SM, PC>;
  using GMRES = Eigen::GMRES<SM, PC>;

  T::N_TIMES(AUT::CG, C, M, [&A, &x, &b] { return run_solver<CG>(A, x, b); });
  T::N_TIMES(AUT::GMRES, C, M,
             [&A, &x, &b] { return run_solver<GMRES>(A, x, b); });
  T::N_TIMES(AUT::BIC, C, M, [&A, &x, &b] { return run_solver<BIC>(A, x, b); });
}

struct eigen_test : public Test<SM, Vec> {
  using super = Test<SM, Vec>;

public:
  eigen_test(int size_) : Test(size_) {}

  const char *name() const {
#ifdef EIGEN_USE_MKL
    return "EigenMKL";
#else
    return "Eigen";
#endif
  }

  void run(recordings &recs, test_collection_t &collection, test_methods &);

  comp sparse_MUL(recordings &);
  comp sparse_LU(recordings &);
  comp parilu();
#ifdef EIGEN_USE_MKL
  comp sparse_PARDISOLU(recordings &);
#endif

  template <class PC>
  void test_iterative(recordings &recs, test_collection_t &collection,
                      test_methods &methods);
};

} // namespace eigen
