#pragma once
#include "dprint.hpp"
#include "factor_matrix.hpp"
#include <boost/filesystem.hpp>
#include <set>
#include <string>
#include <thread>

struct config {

  size_t MAX_FILES = 1;
  size_t RAND_SIZE = -1;
  size_t N_MAX = 0;
  size_t RUN_NTIMES = 1;
  size_t MAX_ITERATIONS = 250;
  size_t MAX_THREADS = std::thread::hardware_concurrency();
  size_t stepping_start = 0, stepping_end = 1, stepping_step = 1;

  bool DRY = false;
  bool APPEND_MODE = false;
  bool SAFE = false;
  bool STORE_PATTERN = false;
  bool VERBOSE = false;
  bool range_test = false;

  double FILL = .1;
  double TOLERANCE = 1e-12;

  static std::set<std::string> hidden;
  std::string PRECOND = "diag";
  std::string path, out_path, out_dir, home, out_name = "";

  config() {
    const char *h = getenv("HOME");
    if (h != nullptr) {
      home = std::string(h);
    } else {
      throw std::runtime_error("Could not expand environment variable 'HOME'.");
    }
  }

  bool operator()(int argc, char **argv) {
    using namespace std;

    bool ilut_s_set = false;

    if (argc > 1) {
      for (int i = 1; i < argc; i++) {
        auto s = string(argv[i]);
        if (s[0] == '-') {
          if (s == "-dry") {
            DRY = true;
          } else if (s == "-a") {
            APPEND_MODE = true;
          } else if (s == "-v") {
            VERBOSE = true;
          } else if (s == "-q") {
#ifdef USE_QT
            STORE_PATTERN = true;
#else
            dprint_err(
                "Cannot store matrix pattern as QT is not compiled into me.");
#endif
          } else if (s == "-safe") {
            SAFE = true;
          } else if (s == "-n" and i + 1 <= argc) {
            RUN_NTIMES = stoul(argv[i + 1]);
            i++;
          } else if (s == "-m" and i + 1 <= argc) {
            MAX_FILES = stoul(argv[i + 1]);
            i++;
          } else if (s == "-r" and i + 1 <= argc) {
            stepping_start = RAND_SIZE = stoul(argv[i + 1]);
            stepping_end = RAND_SIZE + 1;
            range_test = false;
            i++;
          } else if (s == "-f" and i + 1 <= argc) {
            FILL = stod(argv[i + 1]);
            i++;
          } else if (s == "-N" and i + 1 <= argc) {
            N_MAX = stoul(argv[i + 1]);
            i++;
          } else if (s == "-i" and i + 1 <= argc) {
            MAX_ITERATIONS = stoul(argv[i + 1]);
            i++;
          } else if (s == "-ilut_s" and i + 1 <= argc) {
            TestRepeater::ILUT_S = stod(argv[i + 1]);
            ilut_s_set = true;
            i++;
          } else if (s == "-t" and i + 1 <= argc) {
            TOLERANCE = stod(argv[i + 1]);
            i++;
          } else if (s == "-T" and i + 1 <= argc) {
            MAX_THREADS = stoul(argv[i + 1]);
            i++;
          } else if (s == "-I" and i + 1 <= argc) {
            std::string arg(argv[i + 1]);
            int idx = arg.find(".");
            if (idx != std::string::npos) {
              auto lhs = arg.substr(0, idx);
              auto rhs = arg.substr(idx + 1);
              TestRepeater::specific_masked[lhs].emplace(rhs);
            } else {
              TestRepeater::masked.emplace(arg);
            }
            i++;
          } else if (s == "-O" and i + 1 <= argc) {
            std::string arg(argv[i + 1]);
            int idx = arg.find(".");
            if (idx != std::string::npos) {
              auto lhs = arg.substr(0, idx);
              auto rhs = arg.substr(idx + 1);
              TestRepeater::specific_only[lhs].emplace(rhs);
            } else {
              TestRepeater::only.emplace(arg);
            }
            i++;
          } else if (s == "-H" and i + 1 <= argc) {
            hidden.emplace(argv[i + 1]);
            i++;
          } else if (s == "-p" and i + 1 <= argc) {
            PRECOND = string(argv[i + 1]);
            if (PRECOND != AUT::DIAG and PRECOND != AUT::ILUT and
                PRECOND != AUT::ICC and PRECOND != AUT::PARILU) {
              dprint_err("No such preconditioner: " << PRECOND);
              return false;
            }
            i++;
          } else if (s == "-o" and i + 1 <= argc) {
            out_path = string(argv[i + 1]);
            i++;
          } else if (s == "-name" and i + 1 <= argc) {
            out_name = string(argv[i + 1]);
            i++;
          } else if (s == "-rs" and i + 1 <= argc) {
            RAND_SIZE = stepping_start = stoul(argv[i + 1]);
            range_test = true;
            i++;
          } else if (s == "-re" and i + 1 <= argc) {
            stepping_end = stoul(argv[i + 1]);
            range_test = true;
            i++;
          } else if (s == "-step" and i + 1 <= argc) {
            stepping_step = stoul(argv[i + 1]);
            i++;
          } else if (s == "-nospd") {
            TestRepeater::NO_SPD = true;
          }
        } else {
          int ix = path.find("~");
          if (ix != std::string::npos) {
            path.replace(ix, 1, home);
          }
          path = s;
        }
      }
      if (range_test and stepping_end == 0) {
        dprint_err("Need also end for range test.");
        return false;
      }
    }

    out_dir = home + "/Desktop/factor_matrix/";
    if (!out_path.empty()) {
      out_dir = out_path;
      out_path = "";
    }

    TestRepeater::RUN_NTIMES = RUN_NTIMES;
    TestRepeater::MAX_ITERATIONS = MAX_ITERATIONS;
    TestRepeater::TOLERANCE = TOLERANCE;
    TestRepeater::MAX_THREADS = MAX_THREADS;
    TestRepeater::PRECOND = PRECOND;
    TestRepeater::SAFE = SAFE;
    TestRepeater::VERBOSE = VERBOSE;
    if (!ilut_s_set) {
      TestRepeater::ILUT_S = FILL;
    }

    dprint_blue("+ CONFIGURATION");
    dprint_x("|- RUN_NTIMES    : " << RUN_NTIMES);
    dprint_x("|- MAX_FILES     : " << MAX_FILES);
    dprint_x("|- TOLERANCE     : " << TOLERANCE);
    dprint_x("|- MAX_ITERATIONS: " << MAX_ITERATIONS);
    dprint_x("|- MAX_THREADS   : " << MAX_THREADS);
    dprint_x("|- FILL          : " << FILL);
    dprint_x("|- ILUT_S        : " << TestRepeater::ILUT_S);
    dprint_x("|- SAFE          : " << SAFE);
    dprint_x("|- PRECOND       : " << PRECOND);
    dprint_x("|- OUT_DIR       : " << out_dir);

    return true;
  }
};
