#pragma once
#include <map>
#include <vector>

using stl_vec = std::vector<double>;
using stl_matrix = std::vector<std::map<int, double>>;
