#include "run_test.hpp"
#include "recordings.hpp"
#include "string_utility.hpp"
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

std::set<std::string> config::hidden;

void assert_consistency(recordings &R) {
  if (!TestRepeater::SAFE) {
    return;
  }
#ifdef USE_SPOOLES
  eigen::eigen_test et(R.system_size);
  et.set(R);
  original::original_test ot(R.system_size);
  ot.set(R);

  auto InpA = ot.A();
  double *dvec = InpMtx_dvec(InpA);
  int *ivec1 = InpMtx_ivec1(InpA);
  int *ivec2 = InpMtx_ivec2(InpA);
  // InpMtx_writeForHumanEye(ot._A, stdout);
  // dprint(std::setw(8) << et.A);
  assert_is_symmetric(et.A(), R.system_size);

  int q = -1, p = -1;
  bool dothrow = false;
  for (int ii = 0; ii < InpA->nent; ii++) {
    int r = ivec1[ii];
    int c = ivec2[ii];
    double val = dvec[ii];

    if (!is_zero(et.A().coeff(r, c) - val)) {
      dothrow = true;
      q = r;
      p = c;
    }
    if (!is_zero(et.A().coeff(c, r) - val)) {
      dothrow = true;
      q = c;
      p = r;
    }

    if (dothrow) {
      throw std::runtime_error(
          build_string("\e[1;33mWrong value at ", q, ", ", p, " :", val,
                       " != ", et.A().coeff(q, p), "\e[0;97m"));
    }
  }

  dprint("Matrix Conversion Passed.");
#endif
}

void print_run_header(std::ostream &o, config &c) {
  print_system(o);
  o << "# RUN_NTIMES    : " << c.RUN_NTIMES << "\n";
  o << "# MAX_FILES     : " << c.MAX_FILES << "\n";
  o << "# TOLERANCE     : " << c.TOLERANCE << "\n";
  o << "# MAX_ITERATIONS: " << c.MAX_ITERATIONS << "\n";
  o << "# MAX_THREADS   : " << c.MAX_THREADS << "\n";
  o << "# SAFE          : " << c.SAFE << "\n";
  o << "# PRECOND       : " << c.PRECOND << "\n";
  o << "# OUT_DIR       : " << c.out_dir << "\n";
}