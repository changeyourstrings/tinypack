#pragma once
#include <iostream>
#include <sstream>
#include <string>

template <class T> std::ostream &print_args(std::ostream &o, const T &a) {
  return o << a, o;
}

template <class T>
std::ostream &print_args(const char *, std::ostream &o, const T &a) {
  return o << a, o;
}

template <class T, class... Args>
std::ostream &print_args(std::ostream &o, const T &a, const Args &... args) {
  o << a << " ";
  return print_args(o, args...);
}

template <class T, class... Args>
std::ostream &print_args(const char *delim, std::ostream &o, const T &a,
                         const Args &... args) {
  o << a << delim;
  return print_args(delim, o, args...);
}

template <class T, class... Args>
std::string build_string_delim(const char *delim, const T &a,
                               const Args &... args) {
  std::stringstream ss;
  print_args(delim, ss, a, args...);
  return ss.str();
}

template <class T, class... Args>
std::string build_string(const T &a, const Args &... args) {
  std::stringstream ss;
  print_args("", ss, a, args...);
  return ss.str();
}
