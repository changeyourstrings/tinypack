#include "eigen.hpp"
#include "generic_is_eigen_parilu.hpp"

using namespace eigen;

void eigen_test::run(recordings &R, test_collection_t &collection,
                     test_methods &methods) {

  using T = TestRepeater;
  dprint_red("EIGEN using threads: " << Eigen::nbThreads());

  measure_set(R, collection, methods);
  T::N_TIMES(AUT::MUL, collection, methods,
             [this, &R] { return sparse_MUL(R); });

#ifdef EIGEN_USE_MKL
  T::N_TIMES(AUT::PARDISOLU, collection, methods,
             [this, &R] { return sparse_PARDISOLU(R); });
#else
  T::N_TIMES(AUT::LU, collection, methods, [this, &R] { return sparse_LU(R); });
#endif

  // T::N_TIMES(AUT::ILUT, collection, methods, [this] {
  //   Eigen::IncompleteLUT<double, typename SM::StorageIndex> ilut;
  //   ms  t = time_it([this, &ilut]() { ilut.compute(A); });
  //   auto error = (Mat(A) - ilut.getLU()).norm();
  //   return comp{t, error};
  // });

  // T::N_TIMES2(AUT::PARILU, collection, methods, &eigen_test::parilu, this);

  if (TestRepeater::PRECOND == "diag" or TestRepeater::PRECOND.empty()) {
    using PC = Eigen::DiagonalPreconditioner<double>;
    eigen::test_iterative<PC>(collection, methods, _A, _x, _b);
  } else if (TestRepeater::PRECOND == AUT::ILUT) {
    using PC = Eigen::IncompleteLUT<double>;
    eigen::test_iterative<PC>(collection, methods, _A, _x, _b);
  } else if (TestRepeater::PRECOND == AUT::ICC) {
    using PC = Eigen::IncompleteCholesky<double>;
    eigen::test_iterative<PC>(collection, methods, _A, _x, _b);
  } else
    throw std::runtime_error("Unkown Preconditioner: " + TestRepeater::PRECOND);
}

comp eigen_test::sparse_MUL(recordings &R) {
  Vec b;
  ms t = time_it([this, &b]() { b = _A * _x; });
  comp c{t, berror_rec(b)};
  return c;
}

comp eigen_test::sparse_LU(recordings &R) {

  SMColMaj A2 = R.to_eigen_SM_ColMajor();
  A2.makeCompressed();

  Eigen::SparseLU<SMColMaj, Eigen::COLAMDOrdering<int>> solver;
  solver.isSymmetric(true);
  Vec x;
  ms t = time_it([this, &solver, &x, &A2]() {
    solver.analyzePattern(A2);
    solver.factorize(A2);
    x = solver.solve(_b);
  });

  comp c{t, berror(x), 0 , xerror(x)};
  return c;
}

comp eigen_test::parilu() {
  generic_is::generic_parilu<SM, SM, SMColMaj, Vec, Mat> pc(_A, _size);
  ms t = time_it([&pc]() { pc.run(); });
  return comp{t, pc.ilures()};
}

#ifdef EIGEN_USE_MKL
comp eigen_test::sparse_PARDISOLU(recordings &R) {
  Eigen::PardisoLU<SM> solver;
  CMapXd b = R.get_b();
  ms t = time_it([this, &solver, &b]() {
    solver.analyzePattern(A);
    solver.factorize(A);
    x = solver.solve(b);
  });

  double error = berror(b);
  comp c(t, error);
  c.set_load(load());
  c.set_sol(sol());
  return c;
}
#endif
