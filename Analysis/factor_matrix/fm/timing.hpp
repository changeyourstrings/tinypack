#pragma once
#include "dprint.hpp"
#include <chrono>

// using timer = std::timer::cpu_timer;
using ms = std::chrono::duration<double, std::milli>;
namespace chrono = std::chrono;
using clk = std::chrono::steady_clock;

template <class F, class... Args> ms time_it(F &&f, Args &&... args) {
  auto s = clk::now();
  f(args...);
  return std::chrono::duration_cast<ms>(clk::now() - s);
}

template <class R, class F, class... Args>
std::pair<R, ms> time_it(F &&f, Args &&... args) {
  auto s = clk::now();
  R r = f(args...);
  auto dt = std::chrono::duration_cast<ms>(clk::now() - s);
  return {r, dt};
}

void print_timer();
