#pragma once
#include "recordings.hpp"
#include "factor_matrix.hpp"
#include "generic_matrix.hpp"

template <class MatrixT_, class VectorT_> struct Test {

  using MatrixT = MatrixT_;
  using VectorT = VectorT_;

protected:
  int _size;

  MatrixT _A;
  VectorT _x, _b;

public:
  Test(int size)
      : _size(size), _x(size), _A(std::move(matrix_init<MatrixT>(size))),
        _b(size) {}
  virtual ~Test() = default;

  virtual void run(recordings &, test_collection_t &, test_methods &) = 0;
  virtual const char *name() const = 0;

  double berror(const VectorT &x) const {
    return my_norm<VectorT>(A() * x - _b);
  }
  double xerror(const VectorT &x) const { return my_norm<VectorT>(_x - x); }
  double berror_rec(const VectorT &b) const { return my_norm<VectorT>(b - _b); }

  inline const MatrixT &A() const { return _A; }
  inline MatrixT &A() { return _A; }

  void set(const recordings &R) {
    to_matrix<MatrixT>(R, _A);
    to_vector<VectorT>(R.get_x(), _x);
    to_vector<VectorT>(R.get_b(), _b);
  }

  void measure_set(recordings &R, test_collection_t &col, test_methods &M) {
    TestRepeater::N_TIMES(AUT::SET, col, M, [this, &R]() {
      ms t = time_it([this, &R]() { to_matrix<MatrixT>(R, A()); });
      return comp(t, 0);
    });
  }

  static size_t max_iterations() { return TestRepeater::MAX_ITERATIONS; }
  static double tolerance() { return TestRepeater::TOLERANCE; }
};