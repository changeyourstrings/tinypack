#pragma once


#include "eigen_types.hpp"
#include <QColor>
#include <QImage>

static void store_mat(const eigen::SM &R, const std::string &filename) {
  dprint("Storing " << filename);
  QImage img(R.rows(), R.cols(), QImage::Format_Grayscale8);
  img.fill(255);

  QImage img_col(R.rows(), R.cols(), QImage::Format_RGB32);
  img_col.fill(0xFF0000FF);

  double max_val = 0;
  for (int i = 0; i < R.rows(); i++) {
    auto it = Eigen::InnerIterator<eigen::SM>(R, i);
    while (it) {
      max_val = std::max(max_val, it.value());
      ++it;
    }
  }

  for (int i = 0; i < R.rows(); i++) {
    auto it = Eigen::InnerIterator<eigen::SM>(R, i);
    while (it) {

      img.setPixelColor(i, it.index(), QColor("black"));
      QColor col = 0xFFFF00FF * it.value() / max_val;
      img_col.setPixelColor(i, it.index(), col);

      ++it;
    }
  }

  img = img.scaled(std::min(R.rows(), 2048L), std::min(2048L, R.cols()),
                   Qt::AspectRatioMode::KeepAspectRatio);
  img_col = img_col.scaled(std::min(R.rows(), 2048L), std::min(2048L, R.cols()),
                           Qt::AspectRatioMode::KeepAspectRatio);
  img.save(filename.c_str());
  img_col.save(("colored_" + filename).c_str());
}

static void store_pattern(const recordings &R, const std::string &filename) {
  dprint("Storing " << filename);
  QImage img(R.system_size, R.system_size, QImage::Format_Grayscale8);
  img.fill(255);

  for (int i = 0; i < R.rows.size(); i++) {
    auto r = R.rows[i];
    auto c = R.cols[i];

    img.setPixelColor(r, c, QColor("black"));
    img.setPixelColor(c, r, QColor("black"));
  }
  img = img.scaled(2048, 2048, Qt::AspectRatioMode::KeepAspectRatio);
  img.save(filename.c_str());
}
