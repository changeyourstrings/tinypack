#include "factor_matrix.hpp"
#include "recordings.hpp"
#include "string_utility.hpp"
#include <Eigen/Eigenvalues>

#include <fstream>
#include <numeric>

using namespace std;

using TR = TestRepeater;
bool TR::SAFE = false;
bool TR::VERBOSE = false;
bool TR::NO_SPD = false;
double TR::TOLERANCE;
double TR::ILUT_S;
map<string, set<string>> TR::specific_masked, TR::specific_only;
set<string> TR::masked, TR::only;
size_t TR::MAX_ITERATIONS;
size_t TR::MAX_THREADS;
size_t TR::RUN_NTIMES = 1;
string TR::cur_test;
string TR::PRECOND = "diag";

bool check_is_positive_def(const eigen::SM &A, int size,
                           std::vector<std::complex<double>> &std_evs) {
  Eigen::EigenSolver<Eigen::MatrixXd> solver;
  solver.compute(Eigen::MatrixXd(A));
  std_evs = std::vector<std::complex<double>>(size);

  bool pos_def = true;
  auto evs = solver.eigenvalues();
  for (int i = 0; i < size; i++) {
#ifdef DEBUG
    dprint("Yay! Eigenvalue [" << i << "] " << evs[i].real() << " > 0 ! ");
#endif
    pos_def &= evs[i].real() > 0 and is_zero(evs[i].imag());
    std_evs[i] = evs[i];
  }

  return pos_def;
}

void assert_is_positive_def(const eigen::SM &A, int size) {

  std::vector<std::complex<double>> std_evs(size);
  dprint_x("Checking Positive Definite ...");

  bool pos_def = check_is_positive_def(A, size, std_evs);
  if (!pos_def) {
    throw runtime_error("Random creation did not produce PosDef system. ");
  }
  dprint_x("Success!");
}

void assert_is_symmetric(const eigen::SM &A, int size) {
  dprint_x("Checking Symmetry ...");

  if (TestRepeater::VERBOSE) {
    dprint_red("Checking symmetry after generation...");
  }

#pragma omp parallel for
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      if (not is_zero(A.coeff(i, j) - A.coeff(j, i))) {
        throw runtime_error(build_string(
            "\e[1;32mNot symmetric at (", i, ", ", j, ") : ", A.coeff(i, j),
            " != ", A.coeff(j, i), "\e[0;97m", "\n"));
      }
    }
  }
  dprint_x("Success!");
}

bool is_zero(double v) {
  return std::abs(v) < std::numeric_limits<double>::epsilon();
}

ostream &operator<<(ostream &o, const pair<const double *, int> &p) {
  auto d = eigen::CMapXd(p.first, p.second);
  o << d.block(0, 0, 10, 1).transpose();
  return o;
}

ostream &operator<<(ostream &o, const comp &c) {
  o << "∆t: " << std::setprecision(8) << c._runtime.count()
    << " ms | ∆err: " << std::setprecision(8) << c._b_error
    << " | its: " << c._iterations << " | x-err: " << std::setprecision(8)
    << c._x_error;
  return o;
}

// ***********************************************************************************************

comp &measurements::operator[](int i) { return _ms[i]; }

const comp &measurements::operator[](int i) const { return _ms[i]; }

comp measurements::mean() const {
  if (_ms.size() == 1) {
    return _ms.back();
  } else {
    comp res = std::accumulate(
        _ms.begin(), _ms.end(),
        comp{ms(), 0, 0, 0} /* important to have only zeros here */);
    return res.mean(_ms.size());
  }
}

ms measurements::stddev_time() const {
  comp m = mean();
  double d = 0.0;
  for (auto x : _ms) {
    double s = (x.runtime() - m.runtime()).count();
    d += s * s;
  }
  return ms(sqrt(d / (_ms.size() - 1)));
}

void measurements::add(comp &&c) { _ms.push_back(c); }

size_t measurements::size() const { return _ms.size(); }

// ***********************************************************************************************

comp::comp(ms runtime_, double error_, size_t iterations_, double x_error_)
    : _runtime(runtime_), _b_error(error_), _iterations(iterations_),
      _x_error(x_error_), _valid(true) {}

comp comp::operator+(const comp &other) const {
  return comp{_runtime + other._runtime,       //
              _b_error + other._b_error,       //
              _iterations + other._iterations, //
              _x_error + other._x_error};
}

comp &comp::operator/(double v) {
  _runtime /= v;
  _b_error /= v;
  _x_error /= v;
  _iterations = (size_t)((double)_iterations / v);
  return *this;
}

std::string comp::to_csv() const {
  return build_string_delim(",", _runtime.count(), _b_error, _x_error,
                            _iterations);
}
comp &comp::mean(size_t s) { return operator/((double)s); }

// ***********************************************************************************************
