#pragma once
#include "Eigen/Sparse"
#include <type_traits>

template <class M> struct is_eigen { static constexpr bool value = false; };

template <> struct is_eigen<Eigen::SparseMatrix<double, Eigen::RowMajor>> {
  static constexpr bool value = true;
};

template <> struct is_eigen<Eigen::SparseMatrix<double, Eigen::ColMajor>> {
  static constexpr bool value = true;
};
