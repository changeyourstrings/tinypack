#include "timing.hpp"
// #include "boost/chrono/process_cpu_clocks.hpp"
#include <chrono>
#include <vector>

void print_timer() {
  using ns = chrono::nanoseconds;
  size_t iters = 1000 * 1000;

  std::vector<size_t> v(iters);
  std::vector<ns> v_ns(iters);

  auto s = clk::now();
  for (int i = 0; i < iters; ++i) {
    // time to compute and duration
    v_ns[i] = chrono::duration_cast<ns>(clk::now() - s);
  }
  auto q = chrono::duration_cast<ns>(clk::now() - s).count();

  size_t min_ = 100000000;
  size_t max_ = 0;
  size_t sum = 0;
  for (int i = 0; i < iters; ++i) {
    v[i] = v_ns[i].count();
    if (i > 0) {
      auto d = v[i] - v[i - 1];
      if (d > 0) {
        min_ = std::min(min_, d);
      }
      max_ = std::max(max_, d);
      sum += d;
    }
  }

  dprint_x("Timing Information:");
  dprint_x("  Clock call time: " << q / iters << " ns");
  dprint_x("  Min. ∆t : " << min_ << " ns");
  dprint_x("  Mean. ∆t : " << sum / iters << " ns");
  dprint_x("  Max. ∆t : " << max_ << " ns");
}