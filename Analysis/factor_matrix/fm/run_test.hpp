#pragma once
#include "factor_matrix.hpp"
#include "recordings.hpp"
#include "string_utility.hpp"
#include "test_config.hpp"
#include <fstream>
#include <map>

void print_run_header(std::ostream &o, config &c);

template <class Tester> class RunTest {
protected:
  test_collection_t all_ms;
  recordings *_r;
  Tester _tester;
  bool printed_csv_header = false;

public:
  RunTest(recordings *r) : _tester(r->system_size), _r(r) {}
  virtual ~RunTest() = default;

  void print_stats(measurements &ms) const {
    using namespace std;
    if (!ms.size()) {
      dprint_err("No measurement provided.");
      return;
    }

    if (TestRepeater::VERBOSE) {
      dprint_F("| Error x    : " << setw(7) << ms[0].x_error() << "\n"
                                 << "| Error b    : " << setw(7)
                                 << ms[0].b_error());
    }

    dprint_F("| Mean (" << TestRepeater::RUN_NTIMES << ")       : " << ms.mean()
                        << " | stdev(" << ms.stddev_time().count() << ") ");
  }

  virtual void print_header(std::ofstream &o) {
    if (!printed_csv_header) {

      print_args(",", o, "SIZE", _r->non_zeros(), _r->system_size,
                 _r->density(), TestRepeater::MAX_ITERATIONS,
                 TestRepeater::TOLERANCE, TestRepeater::RUN_NTIMES,
                 TestRepeater::MAX_THREADS, "\n");
      printed_csv_header = true;
    }
  }

  void print_to(std::ofstream &o) {
    print_args(",", o, "TEST", _tester.name(), "\n");

    print_header(o);

    int sample = 0;
    for (auto [method, ms] : all_ms) {
      print_args(",", o, "METHOD", method, "\n");

      for (measurements &ms2 : ms) {
        auto m = ms2.mean();
        // sample makes no sense here
        print_args(",", o, sample, m.to_csv(), ms2.stddev_time().count(), "\n");
      }
    }
  }

  void run_test(std::ofstream &o) {

    if (config::hidden.find(_tester.name()) != config::hidden.end()) {
      if (TestRepeater::VERBOSE) {
        dprint_blue("| Test [ " << _tester.name()
                                << " ] is hidden and will not be executed.");
      }
      return;
    }

    dprint_blue("+" << std::string(119, '-'));
    dprint_blue("| Test [ " << _tester.name() << " ]");

    TestRepeater::set_current_test(_tester.name());
    test_methods tested_methods;
    _tester.set(*_r);
    _tester.run(*_r, all_ms, tested_methods);
    TestRepeater::clear_test();

    for (auto method : tested_methods) {
      if (not TestRepeater::excluded(method)) {
        dprint_red("| ---  " << method << " --- ");
        print_stats(all_ms[method].back());
      }
    }

    dprint("| --- ");

    if (o.is_open() and tested_methods.size() > 0) {
      print_to(o);
    }
  }
};
