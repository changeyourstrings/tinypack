#pragma once
#include <iomanip>
#include <iostream>
#include <thread>

#define dprint(x) std::cout << x << "\n";
#define dprint_F(x) std::cout << std::setprecision(5) << x << "\n";

// clang-format off
#define dprint_blue(x)    dprint("\e[1;36m" << x << "\e[0;97m");
#define dprint_red(x)     dprint("\e[1;34m" << x << "\e[0;97m");
#define dprint_gr(x)      dprint("\e[1;35m" << x << "\e[0;97m");
#define dprint_x(x)       dprint("\e[1;32m" << x << "\e[0;97m");
#define dprint_err(x)     dprint("\e[1;31m\nERROR:\n\t" << x << "\n\e[0;97m");
#define dprint_warning(x) dprint("\e[1;31m\nWARNING:\n\t" << x << "\n\e[0;97m");
// clang-format on

#ifdef __APPLE__
static const char *__platform = "APPLE";
#elif __linux__
static const char *__platform = "LINUX";
#elif (defined(WIN32) or defined(_WIN32) or                                    \
       defined(__WIN32) and !defined(__CYGWIN__))
static const char *__platform = "WINDOWS";
#else
static const char *__platform = "UNKNOWN";
#endif

#ifdef __clang__
static const char *__compiler = "clang++ " __VERSION__;
#else
#ifdef __INTEL_COMPILER
static const char *__compiler = "icc " __INTEL_COMPILER;
#else
static const char *__compiler = "gcc " __VERSION__;
#endif
#endif

static void print_system() {
  dprint_x("Compiled With: " << __compiler);
  dprint_x("On System    : " << __platform);
  dprint_x("With Cores   : " << std::thread::hardware_concurrency());
}

static void print_system(std::ostream &o) {
  o << "# Compiled With: " << __compiler << "\n";
  o << "# On System    : " << __platform << "\n";
  o << "# With Cores   : " << std::thread::hardware_concurrency() << "\n";
}
