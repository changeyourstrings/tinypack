#include "recordings.hpp"
#include "dprint.hpp"
#include "factor_matrix.hpp"
#include "string_utility.hpp"
#include <boost/filesystem.hpp>
#include <cmath>
#include <map>
#include <random>
#include <set>

namespace fs = boost::filesystem;
using namespace std;

recordings::recordings(int s, int fill_level)
    : system_size(s), cols(s + fill_level), rows(s + fill_level),
      vals(s + fill_level), b(s), x(s) {}

void recordings::set(int i, int r, int c, double v) {
  if (i < rows.size()) {
    rows[i] = r;
    cols[i] = c;
    vals[i] = v;
  } else
    throw runtime_error("Recordings do not have right size");
}

void recordings::push(int r, int c, double v) {
  rows.push_back(r);
  cols.push_back(c);
  vals.push_back(v);
}

double recordings::density() const {
  return ((double)non_zeros()) / ((double)system_size * system_size);
}

size_t recordings::non_zeros() const { return vals.size() * 2 - system_size; }

bool recordings::load(const string &filename, int fnum, recordings &R,
                      int N_MAX) {
  dprint("At file:" << filename);

  fs::path file(filename);
  if (!fs::exists(file)) {
    dprint_err("File " << file << " does not exist.");
    return false;
  }

  read_spooles_matlab_output(filename, R.rows, R.cols, R.vals, R.system_size,
                             N_MAX);

  auto base_path = file.parent_path().string();
  R.b = read_spooles_matlab_output_vector(build_string(base_path, "/b_", fnum),
                                          'b', N_MAX);

  R.x = read_spooles_matlab_output_vector(build_string(base_path, "/x_", fnum),
                                          'x', N_MAX);

  if (R.x.size() != R.system_size or R.b.size() != R.system_size) {
    throw runtime_error(build_string("Input file ", filename,
                                     " contains errors. The system size does "
                                     "not match the vector size. "));
  }
  R.loaded_from_file = true;
  return true;
}

void recordings::random(int size, double fill, recordings &R) {
  recordings::random2(size, fill, R);
}

using ip = pair<int, int>;
template <> struct std::less<ip> {
  int operator()(const ip &a, const ip &b) const {
    if (a.first == b.first) {
      return less<int>()(a.second, b.second);
    }
    return less<int>()(a.first, b.first);
  }
};

void recordings::random2(int size, double fill, recordings &R) {
  random_device rd;
  mt19937 gen(rd());
  uniform_real_distribution<> dis_gt_zero(1e-1, 3);
  uniform_real_distribution<> dis(-3, 3);

  using int_gen = uniform_int_distribution<int>;

  int_gen idis(0, size - 1);
  std::set<ip> indices;

  int fill_level = (int)floor(fill * (double)(size * size - size) / 2.);

  R = recordings(size, fill_level);

  while (indices.size() < fill_level) {
    int r = idis(gen);
    if (r > 0) {
      // LOWER TRIANGLE IS FILLED
      int_gen icol(0, r - 1);
      int c = icol(gen);
      indices.insert({r, c});
    }
  }

  int i = 0;
  for (auto &[r, c] : indices) {
    R.set(size + i, r, c, dis(gen));
    i++;
  }

  auto Q = R.to_map(true);
  for (int i = 0; i < size; i++) {
    double v;
    if (TestRepeater::NO_SPD) {
      v = dis_gt_zero(gen) + 5;
    } else {
      // should make it positive definite
      double sum =
          std::accumulate(Q[i].begin(), Q[i].end(), 0.0,
                          [](double b, const std::pair<int, double> &a) {
                            return std::abs(a.second) + b;
                          });

      uniform_real_distribution<> row_dist(sum + .1,
                                           sum + .1 + abs(dis(gen) * dis(gen)));
      v = row_dist(gen);
    }
    R.set(i, i, i, v);

    // random solution
    R.x[i] = dis(gen);
  }

  if (TestRepeater::VERBOSE) {
    dprint_blue("Generated random system of size " << size);
  }

  eigen::SM A = R.to_eigen_SM();

  double det = Eigen::MatrixXd(A).determinant();

  assert(!is_zero(det));

  eigen::CMapXd _x = R.get_x_eigen();
  eigen::Vec b = A * _x;

  copy(b.data(), b.data() + size, R.b.data());

  if (TestRepeater::SAFE) { // check symmetry and system validity

    assert_is_symmetric(A, size);
    assert_is_positive_def(A, size);

    if (!is_zero((A * _x - b).norm())) {
      throw runtime_error("Random creation did not produce a valid system. ");
    }
  }
}

eigen::SMColMaj recordings::to_eigen_SM_ColMajor() const {
  auto ts = to_triplets();
  eigen::SMColMaj X(system_size, system_size);
  X.reserve(ts.size());
  X.setFromTriplets(ts.begin(), ts.end());
  return X;
}

eigen::SM recordings::to_eigen_SM() const {
  auto ts = to_triplets();
  eigen::SM X(system_size, system_size);

  X.reserve(ts.size());
  X.setFromTriplets(ts.begin(), ts.end());

  if (TestRepeater::SAFE) {
    assert_is_symmetric(X, system_size);
  }
  return X;
}

stl_matrix recordings::to_map(bool force_skip) const {
  stl_matrix m(system_size);

  for (int i = 0; i < rows.size(); i++) {
    int r = rows[i];
    int c = cols[i];
    double v = vals[i];
    m[r][c] = v;
    if (r != c) {
      m[c][r] = v;
    }
  }

  if (!force_skip and TestRepeater::SAFE) {
    int s = 0;
    for (int i = 0; i < system_size; i++) {
      for (auto &[c, v] : m[i]) {
        assert(m[c].find(i) != m[c].end());
        assert(is_zero(v - m[c][i]));
      }
      s += m[i].size();
    }
    assert(s == 2 * rows.size() - system_size);
  }
  return m;
}

vector<eigen::Triplet> recordings::to_triplets() const {
  vector<eigen::Triplet> ts;
  ts.reserve(non_zeros());

  for (int i = 0; i < rows.size(); i++) {
    ts.emplace_back(rows[i], cols[i], vals[i]);

    if (rows[i] != cols[i]) {
      ts.emplace_back(cols[i], rows[i], vals[i]);
    }
  }

  assert(ts.size() == non_zeros());
  return ts;
}

// ***********************************************************************************************

int extract_size(const string &line, int &e) {
  while (line[e] > '9' or line[e] < '0') {
    e++;
  }
  int y = e;
  while (e < line.size() and (line[e] <= '9' or line[e] >= '0')) {
    e++;
  }
  return stoi(line.substr(y, e - y));
}

stl_vec read_spooles_matlab_output_vector(const string &fp, char pre,
                                          int n_max) {
  ifstream in(fp);

  if (!in.is_open()) {
    dprint_F("Could not open matrix: " << fp);
    return {};
  }

  string line;
  stl_vec vec;
  while (getline(in, line)) {
    if (line.empty()) {
      continue;
    } else {
      if (line[1] == pre) {
        int e = 0;
        while (line[e] != '=') {
          e++;
        }
        vec.push_back(read_value(line, e));
        if (n_max != -1 and vec.size() == n_max) {
          break;
        }
      }
    }
  }
  return vec;
}

int read_spooles_matlab_output(const string &fp, vector<int> &cols,
                               vector<int> &rows, stl_vec &vals, int &size,
                               int n_max) {
  ifstream in(fp);

  if (!in.is_open()) {
    dprint_F("Could not open matrix: " << fp);
    return 0;
  }

  string line;
  int at = 0;
  while (getline(in, line)) {
    if (line.empty()) {
      continue;
    } else {
      int e = line.find("size");
      if (e != string::npos) {
        size = extract_size(line, e);
        continue;
      }

      if (line[1] == 'A') {
        int e;
        auto rc = get_row_and_col(line, e);

        // STORED IN COLUMN MAJOR !
        // IN MATLAB NOTATION: -1
        int c = stoi(rc.first) - 1;  // We're in matlab notation!
        int r = stoi(rc.second) - 1; // We're in matlab notation!

        if (n_max != -1 and (r >= n_max or c >= n_max)) {
          size = n_max;
          continue;
        }

        double v = read_value(line, e);
        rows.push_back(r);
        cols.push_back(c);
        vals.push_back(v);
      }
    }
  }

  return 1;
}

double read_value(string &line, int &e) {
  while ((line[e] > '9' or line[e] < '0') and line[e] != '-') {
    e++;
  }
  int i = e;
  while (line[e] != ';') {
    e++;
  }
  return stod(line.substr(i, e - i));
}

pair<string, string> get_row_and_col(string &line, int &e) {
  string row;
  int i = 3;
  e = 3;
  while (line[e] != ',') {
    e++;
  }
  row = line.substr(i, e - i);
  e++; // eat comma
  i = e;
  while (line[e] != ')') {
    e++;
  }

  return {row, line.substr(i, e - i)};
}
