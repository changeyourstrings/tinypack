#pragma once

#include "dprint.hpp"
#include "timing.hpp"
#include <chrono>
#include <iostream>
#include <random>
#include <utility>

#define HundredTimes for (int __i = 0; __i < 100; __i++)

std::random_device r;
std::default_random_engine e(r());
std::uniform_real_distribution<double> uniform_dist(0, 10);

constexpr int S = 5000;

void init_matrix(double *M) {
  for (int i = 0; i < S; i++) {
    for (int j = 0; j < S; j++) {
      auto idx = S * i + j;
      M[idx] = uniform_dist(e);
    }
  }
}

void init_matrices(double *M, double *N) {
  init_matrix(M);
  init_matrix(N);
}

void mult_mat(double *M, double *N) {
  double *K = new double[S * S];

  for (int k = 0; k < S; k++) {
    for (int i = 0; i < S; i++) {
      K[S * k + i] = 0;
      for (int j = 0; j < S; j++) {
        K[S * k + i] += M[S * k + j] * N[S * j + k];
      }
    }
  }

  M = K;
}

void mult_mat_ru(double *M, double *N, double *K) {
  for (int k = 0; k < S; k++) {
    for (int i = 0; i < S; i++) {
      K[S * k + i] = 0;
      for (int j = 0; j < S; j++) {
        K[S * k + i] += M[S * k + j] * N[S * j + k];
      }
    }
  }
}

double *base_test(unsigned long times) {
  double *M = new double[S * S];
  double *N = new double[S * S];
  double *K = new double[S * S];
  double *Q = new double[S * S];
  init_matrices(M, N);

  for (int k = 0; k < times; k++) {
    mult_mat_ru(M, N, K);
    // swap matrices
    for (int i = 0; i < S * S; i++) {
      Q[i] += K[i];
    }
  }

  std::cout << "matrices multiplied" << std::endl;
  delete[] M;
  delete[] N;
  delete[] K;
  return Q;
}