#pragma once
#include <Eigen/Dense>
#include <Eigen/Sparse>

namespace eigen {

using SM = Eigen::SparseMatrix<double, Eigen::RowMajor>;
using SMColMaj = Eigen::SparseMatrix<double, Eigen::ColMajor>;
using Vec = Eigen::VectorXd;
using Mat = Eigen::MatrixXd;
using MapXd = Eigen::Map<Vec>;
using CMapXd = Eigen::Map<const Vec>;
using Triplet = Eigen::Triplet<double>;

} // namespace eigen