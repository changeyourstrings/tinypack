#include "numerical_libtest.hpp"
#include "dprint.hpp"

namespace fs = boost::filesystem;
int NumericLibTest::setup() {

  using namespace std;
  if (N == -1 and !C.range_test and C.path.empty()) {
    dprint_err("I need either a sane size or a stepping specification. ");
    return -1;
  }

  auto test_type = C.RAND_SIZE == -1 ? "sample" : "size";
  std::string scenario;
  if (C.RAND_SIZE == -1) {
    scenario =
        fs::path(C.path).remove_trailing_separator().leaf().string() + "_";
  }

#if HAVE_OPENCL
  scenario += "_opencl";
#endif

  out_path = build_string(out_dir, test_type, "_", TestRepeater::PRECOND, "_",
                          __platform, "_", scenario, C.out_name, ".csv");

  if (!C.home.empty()) {
    if (!fs::is_directory(string(out_dir))) {
      fs::create_directory(out_dir);
    }

    if (fs::exists(out_path) and not C.APPEND_MODE) {

      std::time_t t = std::time(nullptr);
      std::tm *now = std::localtime(&t);
      // clang-format off
      out_path = build_string(out_dir,
                              test_type, "_",
                              TestRepeater::PRECOND, "_",
                              __platform, "_", 
                              C.out_name, "_",
                              now->tm_year + 1900, "-",
                              now->tm_mon + 1, "-",
                              now->tm_mday, "_",
                              now->tm_hour, "-",
                              now->tm_min, ".csv");
      // clang-format on
    }
    out.open(out_path, C.APPEND_MODE ? ios_base::app : ios_base::out);
  }
  if (out.is_open()) {
    print_run_header(out, C);
  }
  file_counter = j = repeats = 0;
  return 0;
}

void NumericLibTest::get_recordings(int step, recordings &R) {
  if (C.path.empty()) {
    recordings::random(C.range_test ? step : N, C.FILL, R);
  } else {
    recordings::load(build_string(C.path, "/A_", file_counter), file_counter,
                     R);
    file_counter++;
  }
}
