#include "vienna_cl.hpp"
#include "vienna_cl_conv.hpp"

#include "viennacl/linalg/bicgstab.hpp"
#include "viennacl/linalg/cg.hpp"
#include "viennacl/linalg/gmres.hpp"
#include "viennacl/linalg/norm_2.hpp"
#include "viennacl/linalg/prod.hpp"
#include "viennacl/matrix.hpp"

#if VIENNACL_WITH_OPENCL
#include "viennacl/linalg/spai.hpp"
#include "viennacl/ocl/backend.hpp"
#include "viennacl/ocl/context.hpp"
#include "viennacl/ocl/device.hpp"
#include "viennacl/ocl/platform.hpp"
#endif

#include <vector>

template <class T> T max(const T &a, const T &b) { return a > b ? a : b; }

// ********************************************************************** //

namespace vcl {

vcl_test::vcl_test(int size) : Test(size) {
#if VIENNACL_WITH_OPENCL
  auto ctx = viennacl::ocl::current_context();
  std::vector<viennacl::ocl::device> devices = ctx.devices();

  dprint_x("Current context Device:");
  for (auto d : devices) {
    dprint_x("device id  : " << d.id());
    dprint_x("available  : " << d.available());
    dprint_x("compiler_available  : " << d.compiler_available());
    dprint_x("is GPU     : " << (d.type() == CL_DEVICE_TYPE_GPU));
    dprint_x("driver version    : " << d.driver_version());
    dprint_x("double_fp_config    : " << d.double_fp_config());
  }
#endif
}

comp vcl_test::mul() {
  V_t q;
  ms t = time_it([this, &q]() { q = viennacl::linalg::prod(_A, _x); });
  double s = q[0];
  return comp(t, 0);
}

void vcl_test::run(recordings &R, test_collection_t &C, test_methods &M) {
  using namespace viennacl;
  using namespace viennacl::linalg;
  using T = TestRepeater;
  T::N_TIMES(AUT::SET, C, M, [this, &R]() {
    M_t Q;
    ms t = time_it([&R, &Q]() { to_matrix<M_t>(R, Q); });
    return comp{t, 0};
  });

  T::N_TIMES2(AUT::MUL, C, M, &vcl_test::mul, this);
  T::N_TIMES2(AUT::ILUT, C, M, &vcl_test::ilut, this);
  T::N_TIMES2(AUT::ILU0, C, M, &vcl_test::ilu0, this);
  T::N_TIMES2(AUT::PARILU, C, M, &vcl_test::chowilu, this);
  T::N_TIMES2(AUT::PARICC, C, M, &vcl_test::chowicc, this);

  T::N_TIMES2(AUT::CG, C, M,
              &vcl_test::test_pcIterative<cg_tag, cg_solver<V_t>>, this);
  T::N_TIMES2(AUT::BIC, C, M,
              &vcl_test::test_pcIterative<bicgstab_tag, bicgstab_solver<V_t>>,
              this);
  T::N_TIMES2(AUT::GMRES, C, M,
              &vcl_test::test_pcIterative<gmres_tag, gmres_solver<V_t>>, this);

#if VIENNACL_WITH_OPENCL
  T::N_TIMES2(AUT::SPAI, C, M, &vcl_test::spai, this);
  T::N_TIMES2(AUT::FSPAI, C, M, &vcl_test::fspai, this);
#endif
}

int vcl_test::get_row_size() const {
  return max(20, (int)(TestRepeater::ILUT_S * _A.size1()));
}

comp vcl_test::ilut() {
  using namespace viennacl;
  using namespace viennacl::linalg;
  std::unique_ptr<ilut_precond<M_t>> P;

  ms t = time_it([this, &P]() {
    P = std::make_unique<ilut_precond<M_t>>(_A, ilut_tag(get_row_size()));
  });

  auto [L, U] = P->get_LU();
  auto Le = conv(L, true);
  auto Ue = conv(U);
  if (TestRepeater::SAFE) {
    dprint(Le);
    dprint(Ue);
  }
  return comp{t, error_LU(Le, Ue, conv(_A))};
}

comp vcl_test::ilu0() {
  using namespace viennacl;
  using namespace viennacl::linalg;
  std::unique_ptr<ilu0_precond<M_t>> P;

  ms t = time_it([this, &P]() {
    P = std::make_unique<ilu0_precond<M_t>>(_A, ilu0_tag(true));
  });

  eigen::SM LU = conv(P->get_LU());

  return comp{t, error_LU(LU.triangularView<Eigen::Lower>(),
                          LU.triangularView<Eigen::Upper>(), conv(_A))};
}

// comp vcl_test::ilublock() {
//   using namespace viennacl;
//   using namespace viennacl::linalg;
//   std::unique_ptr<block_ilu_precond<M_t, ilut_tag>> P;

//   ms t = time_it([this, &P]() {
//     P = std::make_unique<block_ilu_precond<M_t, ilut_tag>>(
//         _A, ilut_tag(get_row_size()));
//   });

//   auto [L, U] = P->get_LU();

//   return comp{t, error_LU(conv(L, true), conv(U), conv(_A))};
// }

comp vcl_test::chowilu() {
  using namespace viennacl;
  using namespace viennacl::linalg;
  std::unique_ptr<chow_patel_ilu_precond<M_t>> P;

  try {

    ms t = time_it([this, &P]() {
      P = std::make_unique<chow_patel_ilu_precond<M_t>>(
          _A, chow_patel_tag(sweeps, jacobi_iters));
    });

    eigen::SM L = conv(P->L_, P->diag_L_);
    eigen::SM U = conv(P->U_, P->diag_U_);
    eigen::SM A = conv(_A);

    return comp{t, error_LU(L, U, A), (size_t)sweeps};
  } catch (std::runtime_error &x) {
    dprint_err("Chow Patel ILU Error.");
    return comp{ms(0), std::numeric_limits<double>::infinity()};
  }
}

comp vcl_test::chowicc() {
  using namespace viennacl;
  using namespace viennacl::linalg;
  std::unique_ptr<chow_patel_icc_precond<M_t>> P;

  try {
    ms t = time_it([this, &P]() {
      P = std::make_unique<chow_patel_icc_precond<M_t>>(
          _A, chow_patel_tag(sweeps, jacobi_iters));
    });

    eigen::SM L = conv(P->L_, P->diag_L_);
    eigen::SM U = conv(P->L_trans_, P->diag_L_);
    eigen::SM A = conv(_A);

    return comp{t, error_LU(L, U, A), (size_t)sweeps};
  } catch (std::runtime_error &x) {
    dprint_err("Chow Patel ICC Error.");
    return comp{ms(0), std::numeric_limits<double>::infinity()};
  }
}

#if VIENNACL_WITH_OPENCL

comp vcl_test::spai() {
  using namespace viennacl;
  using namespace viennacl::linalg;

  std::unique_ptr<spai_precond<BoostCM>> P;
  BoostCM A = conv2BoostCM(_A);

  ms t = time_it([this, &P, &A]() {
    P = std::make_unique<spai_precond<BoostCM>>(A, spai_tag(1e-3, sweeps));
  });

  return comp{t, error_SPAI(_size, _A, P->spai_m_), (size_t)sweeps};
}

comp vcl_test::fspai() {
  using namespace viennacl;
  using namespace viennacl::linalg;

  std::unique_ptr<fspai_precond<BoostCM>> P;
  BoostCM A = conv2BoostCM(_A);

  ms t = time_it([this, &P, &A]() {
    P = std::make_unique<fspai_precond<BoostCM>>(A, fspai_tag(1e-3, sweeps));
  });

  return comp{
      t,
      std::min(error_SPAI(_size, _A, P->L), error_SPAI(_size, _A, P->L_trans)),
      (size_t)sweeps};
}
#endif
} // namespace vcl
