#include "numerical_libtest.hpp"

int main(int argc, char **argv) {

  print_system();
  print_timer();
#if USE_VCL
  vcl::print_devices();
#endif
  NumericLibTest test(argc, argv);
  // clang-format off
  return test.exec<original::original_test,
                   ccx::ccx_test,
                   eigen::eigen_test,
                   blaze::blaze_test,
                   vcl::vcl_test,
                   raw::raw_test>();
  // clang-format on
}
