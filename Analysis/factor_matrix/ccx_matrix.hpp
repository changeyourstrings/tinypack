#pragma once
#include "fm.hpp"
#include "spooles_orig/myspooles.h"

namespace ccx {

struct ccx_matrix {
  ccx_matrix(int size, int size2) : ad(size) {}
  ccx_matrix(const recordings &R) {

    const int nnzs = R.rows.size();
    const int nnzs_non_diag = nnzs - R.system_size;

    irow.reserve(nnzs_non_diag);
    au.reserve(nnzs_non_diag);
    icol.reserve(R.system_size);
    ad.reserve(R.system_size);

    std::map<int, std::map<int, double>> M;

    for (int i = 0; i < nnzs; i++) {
      const int &r = R.rows[i];
      const int &c = R.cols[i];
      if (r == c) {
        ad.push_back(R.vals[i]);
      } else {
        if (R.loaded_from_file) {
          // we can only work with lower tri values
          // but somehow r is a column and c a row here -.-
          assert(r <= c);
          M[r][c] = R.vals[i];
        } else {
          M[c][r] = R.vals[i];
        }
      }
    }

    int at = 0;
    for (int i = 0; i < R.system_size; i++) {
      icol.push_back(M[i].size());
      for (auto &[r, v] : M[i]) {
        au.push_back(v);
        irow.push_back(r + 1);
        at++;
      }
    }
  }

  std::vector<double> ad, au;
  std::vector<int> irow, icol;
};
} // namespace ccx

template <>
inline void to_matrix<ccx::ccx_matrix>(const recordings &R,
                                       ccx::ccx_matrix &Q) {
  Q = ccx::ccx_matrix(R);
}