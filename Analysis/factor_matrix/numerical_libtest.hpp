#pragma once

#include "connected_libraries.hpp"
#include "fm/fm.hpp"
#include <iostream>
#include <type_traits>

template <> struct RunTest<void> {
  RunTest(recordings *) {}
  void run_test(std::ofstream &) {}
};

template <class... Args>
typename std::enable_if<sizeof...(Args) == 0>::type run_tests(std::ofstream &,
                                                              recordings &) {}

template <class... Args>
typename std::enable_if<sizeof...(Args) == 0>::type
run_tests_with(std::ofstream &, recordings &) {}

template <class T, class... Args>
void run_tests(std::ofstream &o, recordings &R) {
  RunTest<T> rt(&R);
  rt.run_test(o);
  run_tests<Args...>(o, R);
}

template <class RunTest_t, class... Args>
void run_tests_with(std::ofstream &o, recordings &R) {
  RunTest_t rt(&R);
  rt.run_test(o);
  run_tests_with<Args...>(o, R);
}

class NumericLibTest {
  config C;
  std::string path, out_path, out_dir;
  std::ofstream out;

  size_t file_counter = 0;
  size_t j = 0;
  size_t repeats = 0;
  size_t N = 0;

public:
  NumericLibTest(int argc, char **argv) {
    C(argc, argv);
    Eigen::setNbThreads(C.MAX_THREADS);
    path = C.path, out_path = C.out_path, out_dir = C.out_dir;
    N = C.RAND_SIZE;
  }

  int setup();

  void get_recordings(int step, recordings &R);

  template <class... Args> int exec() {
    using namespace std;

    int res = setup();
    if (res != 0) {
      return res;
    }

    // clang-format off
    for (size_t step =  C.stepping_start; 
                step <  C.stepping_end;
                step += C.stepping_step) { // clang-format on

#ifdef DEBUG
      dprint_warning("THIS IS A DEBUG BUILD.");
#endif
      recordings R, R2;
      get_recordings(step, R);

      dprint_gr("+ TESTCASE [ " << step << " ]");
      dprint_red("|  #Nonzeros  : " << R.non_zeros());
      dprint_red("|  b.size     : " << R.system_size);
      dprint_red("|  A.size     : " << R.system_size << " x " << R.system_size);
      dprint_red("|  Density    : " << R.density());

      if (C.DRY) {
        dprint_red("Dry: loaded file: " << file_counter);
        continue;
      }

      try {
        run_tests<Args...>(out, R);
        out.flush();
      } catch (std::exception &x) {
        dprint_err(x.what());
        dprint_err("Finished with Error.");
        return -1;
      }

      if (C.STORE_PATTERN) {
        R2 = std::move(R);
      }

      if (C.N_MAX != 0) {
        repeats++;
        if (repeats == C.N_MAX) {
          break;
        } else {
          step -= C.stepping_step;
        }
      }
    }

    dprint_red("Finished Successfully");
    return 0;
  }
};
