#pragma once

#define UNUSEDTEST(x)                                                          \
  namespace x {                                                                \
  using x##_test = void;                                                       \
  }

#if USE_EIGEN
#include "eigen.hpp"
#else
UNUSEDTEST(eigen);
#endif

#if USE_EIGEN && USE_VCL
#include "eigen_vcl_hybrid.hpp"
#else
UNUSEDTEST(eigen_vcl_hybrid);
#endif

#if USE_BLAZE
#ifndef EIGEN_USE_MKL
#include "blaze.hpp"
#else
UNUSEDTEST(blaze);
#endif
#else
UNUSEDTEST(blaze);
#endif

#if USE_PURE_MKL
// #include "mkl.hpp"
#else
#endif

#if USE_SPOOLES
#include "original.hpp"
#else
UNUSEDTEST(original);
#endif

#if USE_CCX
#include "ccx.hpp"
#else
UNUSEDTEST(ccx);
#endif

#if USE_TRILINOS
#include "trilinos.hpp"
#else
UNUSEDTEST(trilinos);
#endif

#if USE_PETSC
#include "petsc.hpp"
#else
UNUSEDTEST(petsc);
#endif

#if USE_VCL
#include "vienna_cl.hpp"
#else
UNUSEDTEST(vcl);
#endif

#ifndef EIGEN_USE_MKL
#include "raw.hpp"
#else
UNUSEDTEST(raw);
#endif

