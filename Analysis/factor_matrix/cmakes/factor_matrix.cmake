set(COMMON_DEFS ${COMMON_DEFS} USE_EIGEN=1)

set(COMMON_SRCS
    fm/eigen.cpp # eigen is mandatory
    fm/factor_matrix.cpp
    fm/run_test.cpp
    fm/recordings.cpp
    fm/timing.cpp)

add_library(fm STATIC ${COMMON_SRCS})
target_compile_definitions(fm PRIVATE ${COMMON_DEFS})
target_link_libraries(fm ${Boost_LIBRARIES})
set(COMMON_FOUND ON)
